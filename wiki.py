import os
import webapp2
import jinja2
import json
import datetime
import re
import random
import string
import hashlib
import hmac
import logging
import time

from google.appengine.api import memcache
from google.appengine.ext import db

template_dir = os.path.join(os.path.dirname(__file__), 'templates')
jinja_env = jinja2.Environment(loader = jinja2.FileSystemLoader(template_dir), autoescape=False)

#################
####Utilities####
#################

html_escape_table = {
    "&": "&amp;",
    '"': "&quot;",
    "'": "&apos;",
    ">": "&gt;",
    "<": "&lt;",
    }

USER_RE = re.compile(r"^[a-zA-Z0-9_-]{3,20}$")
PASSWORD_RE = re.compile(r"^.{3,20}$")
EMAIL_RE = re.compile(r"^[\S]+@[\S]+\.[\S]+$")

def escape_html(s):
    """Produce entities within text."""
    line = ""
    for c in s:
        line = line+ html_escape_table.get(c,c)

    return line.replace("\n", "<br>")

#User Verification

def valid_username(username):
	return USER_RE.match(username)

def is_existing(username):
	existing_names = db.GqlQuery("SELECT name FROM User")
	return username in existing_names

def valid_password(password):
	return PASSWORD_RE.match(password)


def valid_verify(password, verify):
	if password == verify:
		return True

def valid_email(email=""):
	if email:
		return EMAIL_RE.match(email)
	else:
		return True

# User hashing

def make_salt():
    	return ''.join(random.choice(string.letters) for x in xrange(5))
   
def make_userhash(name, pw, salt=None):
	"""used to hash password before making new User entity	pashed_pw|salt"""
    	if not salt:
        	salt = make_salt()
    	h = hmac.new(salt, str(name)+str(pw)).hexdigest()
    	return '%s|%s' % (h, salt)

def make_user_cookie(user_id):
	"""used in header request to set cookie. user_id|hashed_pw"""
	user = User.get_by_id(int(user_id))
	pw_hash = user.pw_hash.split('|')[0]
	return "%s|%s" % (str(user_id), pw_hash)

def verify_user_cookie(user_cookie):
	"""at Welcome, checks if user_id cookie has id == id and hash == hash """
	num = user_cookie.split('|')[0]
	pw_hash= user_cookie.split('|')[1]
	user = User.get_by_id(int(num))
	if user:
		return user.pw_hash.split('|')[0] == pw_hash

def user_from_cookie(user_cookie_str):		# at Welcome, given user_id cookie returns corresponding User entity
	"""at Welcome, given user_id cookie returns corresponding User entity"""
	user_id = int(user_cookie_str.split("|")[0])
	return User.get_by_id(int(user_id))

def verify_login(name, pw):								# used at login
	""" Login verification, checks for existing User with matching hash """
	userdata = db.GqlQuery("SELECT * FROM User WHERE name = :1",name).get()
	if userdata:
		usersalt = str(userdata.pw_hash.split('|')[1])
		input_hash = make_userhash(name, pw, usersalt)
		if input_hash == userdata.pw_hash:
			return True
 

    
#####################
#### Blog Handler####
#####################

class Handler(webapp2.RequestHandler):
	def write(self, *a, **kw):
      		self.response.out.write(*a, **kw)

    	def render_str(self, template, **params):
        	t = jinja_env.get_template(template)
        	return t.render(params)

    	def render(self, template, **kw):
        	self.write(self.render_str(template, **kw))

	def render_json(self, pdict):
		json_front = json.dumps(pdict)
		self.response.headers['Content-Type'] = 'application/json; charset=UTF-8'
         	self.response.out.write(json_front)
		
	def initialize(self, *a, **kw):
		webapp2.RequestHandler.initialize(self, *a, **kw)
		check_user = self.request.cookies.get('user_id')


#####################
#### User Signup ####
#####################

class SignupPage(Handler):
	def render_signup(self, username="", email="", ue="", pwe="", ve="", eer=""):
		self.render("signup.html", username = username, email = email, username_error=ue, password_error = pwe, verify_error = ve, email_error = eer)

	def get(self):
		self.render_signup()

	def post(self):
        	username_error = ""
		password_error=""
		verify_error=""
		email_error=""
		user_username = self.request.get('username')
		user_password = self.request.get('password')
		user_verify = self.request.get('verify')
		user_email = self.request.get('email')

		check = True

		if valid_username(user_username):
			existing = User.gql("WHERE name= :1", user_username).get()	# check if username is already in datastore
			if existing:
				username_error = "That user already exists yo."
				check = False
		else:
			username_error = "That's not a valid username."
			check = False

		if not valid_password(user_password):
			password_error = "That wasn't a valid password."
			check = False
		if not valid_verify(user_password, user_verify):
			verify_error = "Your passwords didn't match."
			check = False
		if not valid_email(user_email):
			email_error = "That's not a valid email."
			check = False
		if is_existing(user_username):
			check = False
	
		if check:
			pw_hash = make_userhash(user_username, user_password)
			new_user = User(name = user_username, pw_hash = pw_hash, email = user_email)
			new_user.put()

			self.response.headers.add_header('set-cookie', 'user_id=%s; Path=/' % str(make_user_cookie(new_user.key().id())))
			
			self.redirect("/")
		else:
			self.render_signup(user_username, user_email, username_error, password_error, verify_error, email_error)

class LoginPage(Handler):
	def render_login(self, username="", error=""):
		self.render("login.html", username = username, login_error = error)

	def get(self):
		self.render_login()

	def post(self):
		login_error = ""
		user_username = self.request.get('username')
		user_password = self.request.get('password')
		
		if verify_login(str(user_username), str(user_password)):
			q = db.GqlQuery("SELECT * FROM User WHERE name = :1", user_username).get()

			self.response.headers.add_header('set-cookie', 'user_id=%s; Path=/' % str(make_user_cookie(q.key().id())))
			self.redirect("/")
		else:
			login_error = "Invalid user information."
			self.render_login(user_username, login_error)

class LogoutPage(Handler):
	def get(self):
		self.response.headers.add_header('Set-Cookie', 'user_id=; Path=/')
		self.redirect("/")


##################
### WIKI STUFF ###
##################

# Main handler
# login|sign up if not logged in
# edit | logout if signed in
class WikiPage(Handler):
	def get(self, page_name):
		if_user = self.check_user()
		content = self.check_url(page_name, if_user)
		self.render("wiki.html", url=page_name, content=content, user=if_user)

	def check_url(self, page_name, user=None):
		page = Page.get_by_key_name(page_name)

		if page:
			return page.content
		else:
			if user:
				new_page = Page(key_name=page_name)
				new_page.put()
				self.redirect('/_edit'+page_name)
			else:
				self.redirect('/login')
	
	def check_user(self):
		user = self.request.cookies.get('user_id')
		if user:
			name = User.get_by_id(int(user.split('|')[0])).name
			logging.info("USERNAME IS %s" % name)
			return name

# Editpage handler
# all other url redirect here - by url mapping
# if user signed in, redirect to form edit
# if user not signed in, redirect to login
# form input saved as content in Page class, url thing saved as name of page (self.request.path.split('_edit/') or sth
class EditPage(Handler):
	def get(self, page_name):
		user = self.check_user()
		page_content = Page.get_by_key_name(page_name).content
		if user:
			if page_content:
				self.render("edit.html", url=page_name, content=page_content, user = user)
			else:
				self.render("edit.html", url=page_name, content="<b>edit me!</b>", user = user)

		else:
			self.redirect('/signup')
	
	def post(self, page_name):
		page = Page.get_by_key_name(page_name)
		page.content = self.request.get('content')
		page.put()
		self.redirect(page_name)
	
	def check_user(self):
		user = self.request.cookies.get('user_id')
		if user:
			name = User.get_by_id(int(user.split('|')[0])).name
			logging.info("USERNAME IS %s" % name)
			return name

#################
### DB MODELS ###
#################

class User(db.Model):
	name = db.StringProperty(required = True)
	pw_hash = db.StringProperty(required = False)
	email = db.StringProperty(required = False)

# User class
# Page class
class Page(db.Model):
	content = db.TextProperty()
# stores content and string for pagename

PAGE_RE = r'(/(?:[a-zA-Z0-9_-]+/?)*)'
app = webapp2.WSGIApplication([(r'/signup/?', SignupPage),
				(r'/login/?', LoginPage),
				(r'/logout/?', LogoutPage),
				('/_edit/?'+PAGE_RE, EditPage),
				(PAGE_RE, WikiPage)
				], debug=True)



